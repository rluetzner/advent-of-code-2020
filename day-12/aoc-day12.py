#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
import numpy as np


orientations = {0: 'N', 90: 'E', 180: 'S', 270: 'W'}


def move(lines, degs = 90, x = 0, y = 0):
    for l in lines:
        c = l[0]
        steps = int(l[1:])
        if c == 'F':
            c = orientations[degs]
        if c == 'E':
            x += steps
        elif c == 'W':
            x -= steps
        elif c == 'N':
            y += steps
        elif c == 'S':
            y -= steps
        elif c == 'L':
            degs -= steps
            degs %= 360
        elif c == 'R':
            degs += steps
            degs %= 360
        # print(f'l: {l}, x: {x}, y: {y}, degs: {degs}')
    return (abs(x), abs(y))


rotr90 = np.array([[0,-1],[1,0]])
rotl90 = np.array([[0,1],[-1,0]])


def movewp(lines):
    x = y = 0
    wx = 10
    wy = 1
    for l in lines:
        c = l[0]
        steps = int(l[1:])
        if c == 'F':
            x += steps * wx
            y += steps * wy
        elif c == 'E':
            wx += steps
        elif c == 'W':
            wx -= steps
        elif c == 'N':
            wy += steps
        elif c == 'S':
            wy -= steps
        elif c == 'L':
            for _ in range(int(steps / 90)):
                wx,wy = np.array([wx,wy]) @ rotl90
        elif c == 'R':
            for _ in range(int(steps / 90)):
                wx,wy = np.array([wx,wy]) @ rotr90
        # print(f'l: {l}, x: {x}, y: {y}, wx: {wx}, wy: {wy}')
    return (abs(x), abs(y))


lines = get_input('input.txt')
x,y = move(lines)
print(f'x: {x}, y: {y}, MH: {x+y}')
x,y = movewp(lines)
print(f'x: {x}, y: {y}, MH: {x+y}')
