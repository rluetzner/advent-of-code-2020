#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
import re


def mask(line):
    return [c for c in line]


def applymask(val, mask):
    dec = int(val)
    b = [c for c in bin(dec).replace('b','').zfill(36)]
    for i in range(len(mask)):
        if mask[i] == 'X':
            continue
        b[i] = mask[i]
    return int(''.join(b), 2)


def solve(lines):
    m = mask(['X'] for _ in range(36))
    mem = {}
    for l in lines:
        c,val = [p.strip() for p in l.split('=')]
        if c == 'mask':
            m = mask(val)
        else:
            match = re.search(r'mem\[([\d]{1,})\]', c)
            idx = int(match[1])
            mem[idx] = applymask(val,m)
    return [mem[m] for m in mem]


def solve2(lines):
    m = mask(['X'] for _ in range(36))
    mem = {}
    for l in lines:
        c,val = [p.strip() for p in l.split('=')]
        if c == 'mask':
            m = mask(val)
        else:
            match = re.search(r'mem\[([\d]{1,})\]', c)
            idx = int(match[1])
            poss_idx = applymask_to_idx(idx,m)
            for i in poss_idx:
                mem[i] = int(val)
    return [mem[m] for m in mem]


def applymask_to_idx(idx, mask):
    dec = int(idx)
    b = [c for c in bin(dec).replace('b','').zfill(36)]
    poss = get_indices(b, mask)
    return [int(''.join(p), 2) for p in poss]


def get_indices(idx, mask):
    no_branches = ''
    for i in range(len(mask)):
        if mask[i] == 'X':
            sub = get_indices(idx[i+1:], mask[i+1:])
            res = []
            for s in sub:
                res.append(f'{no_branches}1{s}')
                res.append(f'{no_branches}0{s}')
            return res
        elif mask[i] == '1':
            no_branches += '1'
        else:
            no_branches += idx[i]
    return [no_branches]



lines = get_input('input.txt')
res = solve(lines)
print(sum([n for n in res]))
res = solve2(lines)
print(sum([n for n in res]))
