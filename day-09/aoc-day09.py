#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def firstinvalid(lines, preamble, cache = {}):
    for i in range(preamble, len(lines)):
        valid = False
        for j in range(i-preamble, i):
            for k in range(j+1, i):
                res = -1
                key = (j,k)
                if key in cache:
                    res = cache[key]
                else:
                    res = lines[j] + lines[k]
                    cache[key] = res
                if res == lines[i]:
                    valid = True
                    # print(f'{lines[i]} = {lines[j]} + {lines[k]}')
                    break
            if valid:
                break
        if not valid:
            return lines[i]


def crack(lines, invalid):
    for i in range(0, len(lines)):
        for j in range(i+1, len(lines)):
            s = sum(lines[i:j])
            if s > invalid:
                break
            elif s == invalid:
                mi = min(lines[i:j])
                ma = max(lines[i:j])
                return mi + ma


lines = [int(i) for i in get_input('input.txt')]
res = firstinvalid(lines, 25)
print(res)
res = crack(lines, res)
print(res)
