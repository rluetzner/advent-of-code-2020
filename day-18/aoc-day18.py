#!/usr/bin/env python

from functools import reduce
import sys
sys.path.append('..')
from libs.helpers import get_input
import re


def match_braces(line):
    opened = 1
    for i in range(1, len(line)):
        c = line[i]
        if c == '(':
            opened += 1
        elif c == ')':
            opened -= 1
        if opened == 0:
            return i


def solve(line):
    stack = []
    num = None
    op = ''
    while line:
        c = line[0]
        if c == '(':
            end = match_braces(line)
            num = solve(line[1:end])
            line = line[end+1:]
            continue
        elif c == '+':
            op = 'add'
        elif c == '*':
            op = 'mul'
        elif c == ' ':
            if stack and num:
                num = num + stack.pop() if op == 'add' else num * stack.pop()
            if num:
                stack.append(num)
            num = None
        else:
            if num:
                num *= 10
                num += int(c)
            else:
                num = int(c)
        line = line[1:]
    return num + stack.pop() if op == 'add' else num * stack.pop()


def solve_advanced(line):

    def resolve_braces(line):
        while '(' in line:
            for i in range(0, len(line)):
                c = line[i]
                if c == '(':
                    end = match_braces(line[i:])
                    end += i
                    res = solve_advanced(line[i+1:end])
                    line = f'{line[0:i]}{res}{line[end+1:]}'
                    break
        return line

    def resolve_add(line):
        rx = r'(\d+ \+ \d+)'
        while '+' in line:
            match = re.search(rx, line)
            if match:
                n1,n2 = [int(n.strip()) for n in match.group(0).split('+')]
                num = n1 + n2
                line = re.sub(rx, str(num), line, 1)
        return line

    def resolve_mul(line):
        if '*' in line:
            nums = [int(n.strip()) for n in line.split('*')]
            return reduce(lambda x,y: x*y, nums, 1)
        else:
            return int(line.strip())


    res = resolve_braces(line)
    res = resolve_add(res)
    return resolve_mul(res)


lines = get_input('input.txt')
res = [solve(l) for l in lines]
final = sum(r for r in res)
print(final)
res = [solve_advanced(l) for l in lines]
final = sum(r for r in res)
print(final)

