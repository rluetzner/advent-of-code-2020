#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


rows = [i for i in range(128)]
columns = [i for i in range(8)]


def row(l, rows):
    if len(rows) == 1:
        return (rows[0], l)
    
    i = int(len(rows)/2)
    if l[0] == 'F':
        return row(l[1:], rows[:i])
    else:
        return row(l[1:], rows[i:])


def col(l, cols):
    if len(cols) == 1:
        return (cols[0], l)
    
    i = int(len(cols)/2)
    if l[0] == 'L':
        return col(l[1:], cols[:i])
    else:
        return col(l[1:], cols[i:])


def seat(ids, ma):
    for i in filter(lambda i: i not in ids, [i for i in range(ma)]):
        if i-1 in ids and i+1 in ids:
            return i


if __name__ == '__main__':
    lines = get_input('input.txt')
    ids = []
    for l in lines:
        r,rem = row(l, rows)
        c,rem = col(rem, columns)
        ids.append(r * 8 + c)
    ma = max(ids)
    print(ma)
    print(seat(ids, ma))

