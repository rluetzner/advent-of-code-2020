# Advent of Code 2021

This repo contains my solutions to the [Advent of Code 2020](https://adventofcode.com/2020).

I got hooked on puzzles by 2021's Advent of Code. Don't expect anything great to come from this.

