#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
from functools import reduce


def solve(ts, buses):
    dt = {}
    for b in buses:
        if ts % b == 0:
            return (b, 0)
        d = int(ts / b) + 1
        dt[d*b] = b
    mind = min([d for d in dt])
    return (dt[mind], mind-ts)


def solve2(buses_with_offsets):
    buses = [b for b in buses_with_offsets]
    M = reduce(lambda x, y: x*y, buses)
    # print(M)
    res = 0
    for mi in buses_with_offsets:
        Mi = int(M / mi)
        ai = buses_with_offsets[mi]
        si = 0
        # print(f'ai = {ai} mi = {mi}')
        while True:
            si += 1
            ri = (1 - si * Mi) / mi
            if int(ri) == ri:
                # print(f'Equation solved: {ri} x {mi} + {si} x {Mi} = 1')
                res += ai * si * Mi
                break
    return res % M


lines = get_input('input.txt')
ts = int(lines[0])
buses = lines[1].split(',')
bs = [int(e) for e in filter(lambda x: x != 'x', buses)]
b,dt = solve(ts,bs)
print(b*dt)
for b in lines[1:]:
    # print(b)
    buses = b.split(',')
    offsets = {}
    for i in range(len(buses)):
        if buses[i] == 'x':
            continue
        offsets[int(buses[i])] = int(buses[i]) - i
    print(offsets)
    res = solve2(offsets)
    print(res)
