#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def solve(lines):
    ma = max(lines)
    copy = [l for l in lines]
    copy.append(ma + 3)
    copy.sort()
    last = 0
    diffone = 0
    diffthree = 0
    for l in copy:
        diff = l - last
        if diff == 1:
            diffone += 1
        elif diff == 3:
            diffthree += 1
        last = l
    return diffone * diffthree


def solve2(lines, cache = {}):
    if len(lines) == 1:
        return 1
    l = lines[0]
    if l in cache:
        return cache[l]
    poss = 0
    for i in range(1, len(lines)):
        diff = lines[i] - l
        if diff > 3:
            break
        poss += solve2(lines[i:])
    cache[l] = poss
    return poss


lines = [int(l) for l in get_input('input.txt')]
res = solve(lines)
print(res)
copy = [l for l in lines]
copy.append(0)
copy.append(max(lines) + 3)
copy.sort()
res = solve2(copy)
print(res)
