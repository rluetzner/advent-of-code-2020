#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import *
from functools import reduce


def findtrees(grid, trajectory, posx):
    x,y = trajectory
    newy = y
    if newy >= len(grid):
        return 0
    newx = (posx + x) % len(grid[0])
    tree = grid[y][newx] == '#'
    res = 1 if tree else 0
    return res + findtrees(grid[newy:], trajectory, newx)


if __name__ == '__main__':
    lines = get_input('input.txt')
    grid = [[c for c in l] for l in lines]
    trees = findtrees(grid, (3,1), 0)
    print(trees)
    trajectories = [(1,1), (3,1), (5,1), (7,1), (1,2)]
    trees = [findtrees(grid, t, 0) for t in trajectories]
    print(reduce(lambda x,y: x*y, trees))
