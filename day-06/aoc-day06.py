#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def groups(lines):
    grps = []
    gp = []
    for l in lines:
        if not l and gp:
            grps.append(gp)
            gp = []
        else:
            gp.append(l)
    grps.append(gp)
    return grps


def solve2(grps):
    all_yes = 0
    for g in grps:
        concat = ''.join(g)
        unique = list(set(c for c in concat))
        for u in unique:
            all_yes += 1 if all(u in subg for subg in g) else 0
    return all_yes


if __name__ == '__main__':
    lines = get_input('input.txt')
    grps = groups(lines)
    concat_groups = [''.join(g) for g in grps]
    unique = [list(set(c for c in g)) for g in concat_groups]
    s = sum(len(u) for u in unique)
    print(s)
    all_yes = solve2(grps)
    print(all_yes)
