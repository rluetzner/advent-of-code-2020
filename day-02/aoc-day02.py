#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
from collections import Counter


def policy(line):
    split = line.split()
    char = split[1]
    split = split[0].split('-')
    return (line[-1], int(split[0]), int(split[1]))


def check(line):
    split = [s.strip() for s in line.split(':')]
    char,mi,ma = policy(split[0])
    c = Counter(split[1])
    return c[char] >= mi and c[char] <= ma


def check2(line):
    split = [s.strip() for s in line.split(':')]
    char,mi,ma = policy(split[0])
    one = split[1][mi-1] == char
    the_other = split[1][ma-1] == char
    return (one or the_other) and not (one and the_other)


if __name__ == '__main__':
    lines = get_input('input.txt')
    print(len(list(l for l in lines if check(l))))
    print(len(list(l for l in lines if check2(l))))
