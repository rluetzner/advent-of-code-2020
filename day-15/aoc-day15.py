#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def solve(line, maxrounds):
    # We're gonna store the last and next to last occurrence
    # as a tuple.
    # -1 means that there is no occurrence.
    rounds = {line[i]:(-1,i) for i in range(len(line))}
    last = line[-1]
    for i in range(len(line), maxrounds):
        if last in rounds and rounds[last][0] != -1:
            # previous-previous, previous ;-)
            pp,p = rounds[last]
            last = p - pp
        else:
            last = 0
        if last in rounds:
            pp,p = rounds[last]
            rounds[last] = (p, i)
        else:
            rounds[last] = (-1, i)
    return last


lines = [[int(c) for c in l.split(',')] for l in get_input('input.txt')]
for l in lines:
    res = solve(l, 2020)
    print(res)
for l in lines:
    res = solve(l, 30000000)
    print(res)
