#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def run(lines, state, visited = []):
    idx, acc = state
    if idx in visited:
        print('Infinite loop detected...')
        return (False, acc)
    visited.append(idx)
    cmd, num = lines[idx].split()
    num = int(num)
    if cmd == 'nop':
        idx += 1
    elif cmd == 'acc':
        acc += num
        idx += 1
    elif cmd == 'jmp':
        idx += num
    if idx >= len(lines):
        print('Terminated successfully...')
        return (True, acc)
    return run(lines, (idx, acc), visited)


def fix(lines):
    for i in range(0, len(lines)):
        l = lines[i]
        lines_copy = []
        if l.startswith('nop'):
            lines_copy = [e for e in lines]
            lines_copy[i] = lines_copy[i].replace('nop', 'jmp')
            print(lines_copy[i])
        elif l.startswith('jmp'):
            lines_copy = [e for e in lines]
            lines_copy[i] = lines_copy[i].replace('jmp', 'nop')
            print(lines_copy[i])
        else:
            # Without a change the code will never run!
            continue
        terminated, acc = run(lines_copy, (0, 0), [])
        if terminated:
            return acc


lines = get_input('input.txt')
_, acc = run(lines, (0, 0))
print(f'Final ACC: {acc}')
acc = fix(lines)
print(f'Final ACC when fixed: {acc}')
