#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def init(lines):
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x] == '#':
                yield((x,y,0))


def neighbours(p):
    px,py,pz = p
    for z in range(pz-1,pz+2):
        for y in range(py-1, py+2):
            for x in range(px-1, px+2):
                if x == px and y == py and z == pz:
                    continue
                yield((x,y,z))

def neighbours_adv(p):
    px,py,pz,pw = p
    for z in range(pz-1,pz+2):
        for y in range(py-1, py+2):
            for x in range(px-1, px+2):
                for w in range(pw-1, pw+2):
                    if x == px and y == py and z == pz and w == pw:
                        continue
                    yield((x,y,z,w))

def step(points, neighbours):
    pois = [n for p in points for n in neighbours(p)]
    pois += points
    newpoints = set()
    for p in pois:
        ns = list(neighbours(p))
        c = list(filter(lambda x: x in points, ns))
        if p in points:
            # Already actice
            if len(c) == 2 or len(c) == 3:
                newpoints.add(p)
        elif len(c) == 3:
            newpoints.add(p)
    return list(newpoints)


def boot(cubes, s, neighbours):
    for i in range(s):
        cubes = step(cubes, neighbours)
    return cubes


lines = get_input('input.txt')
cubes = list(init(lines))
c = boot(cubes, 6, neighbours)
print(len(c))
cubes = [(x,y,z,0) for x,y,z in cubes]
c = boot(cubes, 6, neighbours_adv)
print(len(c))
