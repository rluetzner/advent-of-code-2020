#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def solve(lines):
    for i in range(len(lines)):
        for j in range(i, len(lines)):
            a = lines[i]
            b = lines[j]
            if a + b == 2020:
                return (a, b)


def solve2(lines):
    for i in range(len(lines)):
        for j in range(i, len(lines)):
            for k in range(j, len(lines)):
                a = lines[i]
                b = lines[j]
                c = lines[k]
                if a + b + c == 2020:
                    return (a, b, c)


if __name__ == '__main__':
    lines = get_input('input.txt')
    lines = [int(l) for l in lines]
    a,b = solve(lines)
    print(f'A: {a}, B: {b}')
    print(a*b)
    
    a,b,c = solve2(lines)
    print(f'A: {a}, B: {b}, C: {c}')
    print(a*b*c)
