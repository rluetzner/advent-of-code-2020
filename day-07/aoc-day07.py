#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input


def rules(lines):
    rls = {}
    for l in lines:
        outer, inner = [e.strip().replace('bags', 'bag') for e in l.split('contain')]
        dic = {}
        for i in [i.strip(' .') for i in inner.split(',')]:
            if i.startswith('no'):
                continue
            split = i.split()
            num = int(split[0])
            bag = ' '.join(split[1:])
            dic[bag] = num
        rls[outer] = dic
    return rls


def contains(rls, bag):
    containers = []
    containees = [bag]
    while containees:
        b = containees.pop()
        for r in rls:
            if r in containers:
                continue
            if b in rls[r]:
                containers.append(r)
                if r not in containees:
                    containees.append(r)
    return containers


def innerbags(rls, bag):
    if not rls[bag]:
        return 0
    return sum(rls[bag][r] + rls[bag][r] * innerbags(rls, r) for r in rls[bag])


if __name__ == '__main__':
    lines = get_input('input.txt')
    rls = rules(lines)
    cnt = contains(rls, 'shiny gold bag')
    print(sum(1 for _ in cnt))
    res = innerbags(rls, 'shiny gold bag')
    print(res)
