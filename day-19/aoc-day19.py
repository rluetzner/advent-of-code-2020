#!/usr/bin/env python

import sys
sys.path.append('..')
from libs.helpers import get_input
import re


def rules(lines):
    rls = {}
    for l in lines:
        if not l:
            break
        m = re.search(r'^(\d+): (.*)$', l)
        rls[m.group(1)] = m.group(2)
    return rls


def build_regex(rules):
    unfinished = [r for r in rules]
    while unfinished:
        for u in unfinished:
            rule = rules[u]
            if '"' in rule:
                rules[u] = rule.replace('"','')
                unfinished.remove(u)
                break
            elif '|' in rule:
                # TODO
            else:
                split = [sub.strip() for sub in rule.split()]
                new_rule = ''
                for s in split:



lines = get_input('test.txt')
rls = rules(lines)
print(rls)
